<?php

namespace GetRepo\ExpressionLanguage\Function;

use Symfony\Component\ExpressionLanguage\ExpressionFunction;

class KeysFunction extends ExpressionFunction
{
    public function getName(): string
    {
        return 'keys';
    }

    public function getCompiler(): \Closure
    {
        return fn ($str): string => sprintf('(is_array(%1$s) ? array_keys(%1$s) : [])', $str);
    }

    public function getEvaluator(): \Closure
    {
        return function ($arguments, $var) {
            if (is_array($var)) {
                return array_keys($var);
            }

            return [];
        };
    }
}
