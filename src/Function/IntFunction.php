<?php

namespace GetRepo\ExpressionLanguage\Function;

use Symfony\Component\ExpressionLanguage\ExpressionFunction;

class IntFunction extends ExpressionFunction
{
    public function getName(): string
    {
        return 'int';
    }

    public function getCompiler(): \Closure
    {
        return fn ($str): string => sprintf('intval(%s)', $str);
    }

    public function getEvaluator(): \Closure
    {
        return function ($arguments, $var): int {
            if (is_scalar($var)) {
                return intval($var);
            }

            throw new \InvalidArgumentException(sprintf(
                'int() function expected scalar, got %s',
                get_debug_type($var),
            ));
        };
    }
}
