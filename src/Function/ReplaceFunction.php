<?php

namespace GetRepo\ExpressionLanguage\Function;

use Symfony\Component\ExpressionLanguage\ExpressionFunction;

class ReplaceFunction extends ExpressionFunction
{
    public function getName(): string
    {
        return 'replace';
    }

    public function getCompiler(): \Closure
    {
        return function (): void {
        };
    }

    public function getEvaluator(): \Closure
    {
        return function ($names, $searchOrPattern = null, $replace = null, $subject = null) {
            $subject = (string) $subject;
            if (is_string($searchOrPattern) && false !== @preg_match($searchOrPattern, $subject)) {
                return preg_replace($searchOrPattern, $replace, $subject);
            } else {
                return str_replace($searchOrPattern, $replace, $subject);
            }
        };
    }
}
