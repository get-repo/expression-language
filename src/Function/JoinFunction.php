<?php

namespace GetRepo\ExpressionLanguage\Function;

use Symfony\Component\ExpressionLanguage\ExpressionFunction;

class JoinFunction extends ExpressionFunction
{
    public function getName(): string
    {
        return 'join';
    }

    public function getCompiler(): \Closure
    {
        return fn ($separator, $array): string => sprintf(
            'is_string(%1$s) and is_array(%2$s)',
            $separator,
            $array,
        );
    }

    public function getEvaluator(): \Closure
    {
        return function (array $args, string $separator, array $array): string {
            return implode($separator, $array);
        };
    }
}
