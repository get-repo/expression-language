<?php

namespace GetRepo\ExpressionLanguage\Function;

use Symfony\Component\ExpressionLanguage\ExpressionFunction;

class TrimFunction extends ExpressionFunction
{
    public function getName(): string
    {
        return 'trim';
    }

    public function getCompiler(): \Closure
    {
        return fn ($str): string => sprintf('(is_string(%1$s) ? trim(%1$s) : %1$s)', $str);
    }

    public function getEvaluator(): \Closure
    {
        return function ($arguments, $string, $characters = null) {
            $string = (string) $string;

            return is_null($characters) ? trim($string) : trim($string, (string) $characters);
        };
    }
}
