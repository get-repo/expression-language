<?php

namespace GetRepo\ExpressionLanguage\Function;

use Symfony\Component\ExpressionLanguage\ExpressionFunction;

class LengthFunction extends ExpressionFunction
{
    public function getName(): string
    {
        return 'length';
    }

    public function getCompiler(): \Closure
    {
        return fn ($str): string => sprintf('(is_array(%1$s) ? count(%1$s) : strlen(%1$s))', $str);
    }

    public function getEvaluator(): \Closure
    {
        return function ($arguments, $var) {
            if (is_array($var)) {
                return count($var);
            } elseif (is_object($var)) {
                if ($var instanceof \Countable) {
                    return $var->count();
                }

                return is_countable($var) ? count($var) : 0; // @phpstan-ignore-line
            } else {
                return mb_strlen((string) $var);
            }
        };
    }
}
