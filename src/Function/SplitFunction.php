<?php

namespace GetRepo\ExpressionLanguage\Function;

use Symfony\Component\ExpressionLanguage\ExpressionFunction;

class SplitFunction extends ExpressionFunction
{
    public function getName(): string
    {
        return 'split';
    }

    public function getCompiler(): \Closure
    {
        return fn ($separator, $string, $limit): string => sprintf(
            'is_string(%1$s) and is_string(%2$s) and (is_int(%3$s) or is_null(%3$s))',
            $separator,
            $string,
            $limit,
        );
    }

    public function getEvaluator(): \Closure
    {
        return function (array $args, string $separator, string $string, int $limit = PHP_INT_MAX): array {
            return $separator ? explode($separator, $string, $limit) : [$string];
        };
    }
}
