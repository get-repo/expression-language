<?php

namespace GetRepo\ExpressionLanguage\Function;

use Symfony\Component\ExpressionLanguage\ExpressionFunction;
use Symfony\Component\String\UnicodeString;

class BetweenFunction extends ExpressionFunction
{
    public function getName(): string
    {
        return 'between';
    }

    public function getCompiler(): \Closure
    {
        return fn ($start, $end): string => sprintf('is_string(%s) and is_string(%s)', $start, $end);
    }

    public function getEvaluator(): \Closure
    {
        return function (array $args, string $start, string $end, string $subject): string {
            return (string) (new UnicodeString($subject))
                ->after($start)
                ->before($end);
        };
    }
}
