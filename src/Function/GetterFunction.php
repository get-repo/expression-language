<?php

namespace GetRepo\ExpressionLanguage\Function;

class GetterFunction extends AbstractPropertyAccessorFunction
{
    public function getName(): string
    {
        return 'get';
    }

    public function getEvaluator(): \Closure
    {
        return fn (
            $names,
            $objectOrArray = [],
            $propertyPath = ''
        ) => $this->getPropertyAccessor()->getValue($objectOrArray, $propertyPath);
    }
}
