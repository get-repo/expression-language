<?php

namespace GetRepo\ExpressionLanguage\Function;

use Symfony\Component\ExpressionLanguage\ExpressionFunction;

class DateFunction extends ExpressionFunction
{
    public function getName(): string
    {
        return 'date';
    }

    public function getCompiler(): \Closure
    {
        return fn ($str): string => sprintf('(date(%1$s) ? date_format(%1$s) : %1$s)', $str);
    }

    public function getEvaluator(): \Closure
    {
        return function ($arguments, $var) {
            if (is_scalar($var) && !is_bool($var)) {
                if (false === strtotime($var)) {
                    throw new \InvalidArgumentException(sprintf('date() function invalid date provided "%s"', $var));
                }
                return new \DateTime($var);
            }

            throw new \InvalidArgumentException(sprintf(
                'date() function expected scalar, got %s',
                get_debug_type($var),
            ));
        };
    }
}
