<?php

namespace GetRepo\ExpressionLanguage\Function;

class SetterFunction extends AbstractPropertyAccessorFunction
{
    public function getName(): string
    {
        return 'set';
    }

    public function getEvaluator(): \Closure
    {
        return function ($names, $objectOrArray = [], $propertyPath = '', $value = null) {
            $this->getPropertyAccessor()->setValue($objectOrArray, $propertyPath, $value);

            return $objectOrArray;
        };
    }
}
