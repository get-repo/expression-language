<?php

namespace GetRepo\ExpressionLanguage\Function;

use Symfony\Component\ExpressionLanguage\ExpressionFunction;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

abstract class AbstractPropertyAccessorFunction extends ExpressionFunction
{
    public function getCompiler(): \Closure
    {
        return function (): void {
        };
    }

    protected function getPropertyAccessor(): PropertyAccessorInterface
    {
        $builder = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->enableMagicCall();

        return $builder->getPropertyAccessor();
    }
}
