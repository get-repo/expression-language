<?php

namespace GetRepo\ExpressionLanguage\Function;

use Symfony\Component\ExpressionLanguage\ExpressionFunction;
use Symfony\Component\ExpressionLanguage\SyntaxError;

class RangeFunction extends ExpressionFunction
{
    public function getName(): string
    {
        return 'range';
    }

    public function getCompiler(): \Closure
    {
        return function ($str): string {
            $this->buildRange(trim($str, '"'));

            return sprintf('is_string(%s)', $str);
        };
    }

    public function getEvaluator(): \Closure
    {
        return fn ($arguments, string $var): array => $this->buildRange($var);
    }

    /**
     * @return float[]|int[]|string[]
     */
    private function buildRange(string $value): array
    {
        $range = [];
        foreach (\explode(',', $value) as $exp) {
            // match x-x
            if (\preg_match('/^(\d+)\-(\d+)( step (\d+))?$/', $exp, $matches)) {
                $values = @range(
                    $matches[1], // start
                    $matches[2], // end
                    (float) ($matches[4] ?? 1) // step
                );
                if (!is_array($values)) {
                    $e = $this->getSyntaxErrorException(
                        $exp,
                        $value,
                        (isset($matches[4]) ? 'Maybe check the step in not too high ?' : null)
                    );
                    throw $e;
                }
                $range = [...$range, ...$values];
            } elseif (\preg_match('/^(\d+)$/', $exp, $matches)) {
                $range[] = (int) $matches[1];
            } else {
                throw $this->getSyntaxErrorException($exp, $value);
            }
        }

        return array_values(\array_unique($range));
    }

    private function getSyntaxErrorException(string $rangeExp, string $fullExp, string $help = null): SyntaxError
    {
        $message = "Invalid range `{$rangeExp}`";
        if ($fullExp) {
            $message .= " in expression `{$fullExp}`.";
        }
        if ($help) {
            $message .= "\n{$help}";
        }

        return new SyntaxError($message);
    }
}
