<?php

namespace GetRepo\ExpressionLanguage\Function;

class HasserFunction extends AbstractPropertyAccessorFunction
{
    public function getName(): string
    {
        return 'has';
    }

    public function getEvaluator(): \Closure
    {
        return fn (
            $names,
            $objectOrArray = [],
            $propertyPath = ''
        ) => $this->getPropertyAccessor()->isReadable($objectOrArray, $propertyPath);
    }
}
