<?php

namespace GetRepo\ExpressionLanguage;

use Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface;

class Provider implements ExpressionFunctionProviderInterface
{
    public function getFunctions(): array
    {
        // just fakes args
        $args = ['', fn () => '', fn () => ''];

        return [
            new Function\BetweenFunction(...$args),
            new Function\DateFunction(...$args),
            new Function\GetterFunction(...$args),
            new Function\HasserFunction(...$args),
            new Function\IntFunction(...$args),
            new Function\JoinFunction(...$args),
            new Function\KeysFunction(...$args),
            new Function\LengthFunction(...$args),
            new Function\RangeFunction(...$args),
            new Function\ReplaceFunction(...$args),
            new Function\SetterFunction(...$args),
            new Function\SplitFunction(...$args),
            new Function\TrimFunction(...$args),
        ];
    }
}
