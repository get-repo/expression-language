<?php

namespace Test\Function;

use GetRepo\ExpressionLanguage\ExpressionLanguage;

class SetterTest extends FunctionTestCase
{
    public function setUp(): void
    {
        $this->el = new ExpressionLanguage();
    }

    public function testSet(): void
    {
        $this->assertEquals(
            ['key' => 1, 'new' => true],
            $this->el->evaluate('set(array, "[new]", true)', ['array' => ['key' => 1]])
        );
    }
}
