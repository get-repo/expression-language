<?php

namespace Test\Function;

use GetRepo\ExpressionLanguage\ExpressionLanguage;
use PHPUnit\Framework\Attributes\DataProvider;

class LengthTest extends FunctionTestCase
{
    public function setUp(): void
    {
        $this->el = new ExpressionLanguage();
    }

    public static function providerLength(): array
    {
        $object = new class () implements \Countable {
            public function count(): int
            {
                return 5;
            }
        };

        return [
            // empty
            [0, 'length(null)'],
            [0, 'length(false)'],
            [0, 'length([])'],
            // true
            [1, 'length(true)'],
            // arrays
            [1, 'length([1])'],
            [3, 'length([1, 2, 3])'],
            [1, 'length(["string"])'],
            [0, 'length(array)', ['array' => []]],
            [1, 'length(array)', ['array' => ['key' => 'value']]],
            // strings
            [3, 'length("123")'],
            [0, 'length("")'],
            // object
            [5, 'length(object)', ['object' => $object]],
        ];
    }

    #[DataProvider('providerLength')]
    public function testLength(int $expected, string $expression, array $values = []): void
    {
        $this->assertEquals(
            $expected,
            $this->el->evaluate($expression, $values)
        );
    }
}
