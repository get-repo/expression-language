<?php

namespace Test\Function;

use GetRepo\ExpressionLanguage\ExpressionLanguage;
use PHPUnit\Framework\Attributes\DataProvider;

class ReplaceTest extends FunctionTestCase
{
    public function setUp(): void
    {
        $this->el = new ExpressionLanguage();
    }

    public static function providerReplace(): array
    {
        $values = ['string' => '--find_me--'];

        return [
            // search as string
            ['--find_me--', 'replace("fail", "SHOULD FAIL", string)', $values],
            ['--FOUND--', 'replace("find_me", "FOUND", string)', $values],
            ['--FOUND--', 'replace("/find_me/", "FOUND", string)', $values],
            ['--find_me FOUND--', 'replace("/(find_me)/", "$1 FOUND", string)', $values],
            ['--FOUND--', 'replace("/FIND_ME/i", "FOUND", string)', $values],
            // search or-and replace as arrays
            ['--find_me--', 'replace(["fail"], "SHOULD FAIL", string)', $values],
            ['--find_me--', 'replace(["fail"], ["SHOULD FAIL"], string)', $values],
            ['*find*me*', 'replace(["--", "_"], "*", string)', $values],
            ['+find-me+', 'replace(["--", "_"], ["+", "-"], string)', $values],
        ];
    }

    #[DataProvider('providerReplace')]
    public function testReplace(string $expected, string $expression, array $values = []): void
    {
        $this->assertEquals(
            $expected,
            $this->el->evaluate($expression, $values)
        );
    }
}
