<?php

namespace Test\Function;

use GetRepo\ExpressionLanguage\ExpressionLanguage;
use PHPUnit\Framework\Attributes\DataProvider;

class HasserTest extends FunctionTestCase
{
    public function setUp(): void
    {
        $this->el = new ExpressionLanguage();
    }

    public static function providerHas(): array
    {
        $values = ['array' => ['key' => 1]];

        return [
            [false, 'has(array, "[whatever]")', $values],
            [true, 'has(array, "[key]")', $values],
        ];
    }

    #[DataProvider('providerHas')]
    public function testHas(bool $expected, string $expression, array $values = []): void
    {
        $this->assertEquals(
            $expected,
            $this->el->evaluate($expression, $values)
        );
    }
}
