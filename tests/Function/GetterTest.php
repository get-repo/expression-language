<?php

namespace Test\Function;

use GetRepo\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\PropertyAccess\Exception\NoSuchIndexException;

class GetterTest extends FunctionTestCase
{
    public function setUp(): void
    {
        $this->el = new ExpressionLanguage();
    }

    public function testGetFailure(): void
    {
        $this->expectException(NoSuchIndexException::class);
        $this->assertEquals(
            'should fail anyway',
            $this->el->evaluate('get(array, "[whatever]")', ['array' => ['key' => 1]])
        );
    }

    public function testGetSuccess(): void
    {
        $this->assertEquals(
            1,
            $this->el->evaluate('get(array, "[key]")', ['array' => ['key' => 1]])
        );
    }
}
