<?php

namespace Test\Function;

use GetRepo\ExpressionLanguage\ExpressionLanguage;
use PHPUnit\Framework\Attributes\DataProvider;

class DateTest extends FunctionTestCase
{
    private const DATE_FORMAT = 'Y-m-d H:i:s';

    public function setUp(): void
    {
        $this->el = new ExpressionLanguage();
    }

    public static function providerDateInvalid(): array
    {
        return [
            ['date() function expected scalar, got null', 'date(null)'],
            ['date() function expected scalar, got bool', 'date(false)'],
            ['date() function expected scalar, got bool', 'date(true)'],
            ['date() function expected scalar, got array', 'date([])'],
            ['date() function invalid date provided "156"', 'date("156")'],
            ['date() function invalid date provided "string"', 'date("string")'],
            ['date() function invalid date provided "year 2022"', 'date("year 2022")'],
            ['date() function invalid date provided "1 Jan 2000 999pm"', 'date("1 Jan 2000 999pm")'],
        ];
    }

    #[DataProvider('providerDateInvalid')]
    public function testDateInvalid(string $expectedMessage, string $expression): void
    {
        $this->expectExceptionMessage($expectedMessage);
        $this->el->evaluate($expression);
    }

    public static function providerDateValid(): array
    {
        return [
            ['2000-01-01 00:00:00', 'date("2000-01-01")'],
            ['2001-01-01 00:00:00', 'date("2000-01-01 +1 year")'],
            ['2000-01-01 17:00:00', 'date("1 Jan 2000 5pm")'],
            ['2000-04-30 14:21:00', 'date("04/30/2000 14:21")'], // american format
        ];
    }

    #[DataProvider('providerDateValid')]
    public function testDateValid(string $expected, string $expression): void
    {
        $date = $this->el->evaluate($expression);
        $this->assertInstanceOf(\DateTime::class, $date);
        $this->assertEquals($expected, $date->format(self::DATE_FORMAT));
    }
}
