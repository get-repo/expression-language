<?php

namespace Test\Function;

use GetRepo\ExpressionLanguage\ExpressionLanguage;
use PHPUnit\Framework\Attributes\DataProvider;

class TrimTest extends FunctionTestCase
{
    public function setUp(): void
    {
        $this->el = new ExpressionLanguage();
    }

    public static function providerTrim(): array
    {
        return [
            // empty
            ['', 'trim("")'],
            ['', 'trim(null)'],
            // trim default
            ['aaaaa', 'trim(" aaaaa ")'],
            // trim default
            ['this is a test with TABS', 'trim(" this is a test with TABS     ")'],
            // trim with characters
            ['15.78 ', 'trim("$15.78 ", "$")'],
            ['15.78', 'trim("  $15.78 ", " $")'],
        ];
    }

    #[DataProvider('providerTrim')]
    public function testTrim(string $expected, string $expression): void
    {
        $this->assertEquals(
            $expected,
            $this->el->evaluate($expression)
        );
    }
}
