<?php

namespace Test\Function;

use GetRepo\ExpressionLanguage\ExpressionLanguage;
use PHPUnit\Framework\Attributes\DataProvider;

class RangeTest extends FunctionTestCase
{
    public function setUp(): void
    {
        $this->el = new ExpressionLanguage();
    }

    public static function providerRange(): array
    {
        return [
            // failures
            [
                false,
                'range("0-30 step")',
                [],
                'Invalid range `0-30 step` in expression `0-30 step` around position 0',
            ],
            [
                false,
                'range("1,2,3,WRONG,100,101")',
                [],
                'Invalid range `WRONG` in expression `1,2,3,WRONG,100,101` around position 0',
            ],
            [false, 'range("0-30 step 1000")', [], 'Argument #3 ($step)'], // different error for PHP 8.2 & PHP 8.3
            // success
            [[1], 'range("1")'],
            [[1, 2, 3], 'range("1-3")'],
            [[1, 30, 5], 'range("1,30,5")'],
            [[55, 3, 4, 5, 10], 'range("55,3-5,10")'],
            [[0, 10, 20, 30], 'range("0-30 step 10")'],
            [[10, 20], 'range("10-20 step " ~ step)', ['step' => 10]],
            [[1, 2, 10, 60, 9999], 'range("1,2,10-100 step 50,9999")'],
            [[0, 1, 5, 10, 11], 'range("0-0,0-0,0-1 step 1,0-10 step 5,11")'],
        ];
    }

    #[DataProvider('providerRange')]
    public function testRange(
        bool|array $expected,
        string $expression,
        array $values = [],
        string $exception = null,
    ): void {
        if ($exception) {
            $this->expectExceptionMessage($exception);
        }
        $this->assertEquals(
            $expected,
            $this->el->evaluate($expression, $values)
        );
    }
}
