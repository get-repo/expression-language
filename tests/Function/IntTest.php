<?php

namespace Test\Function;

use GetRepo\ExpressionLanguage\ExpressionLanguage;
use PHPUnit\Framework\Attributes\DataProvider;

class IntTest extends FunctionTestCase
{
    public function setUp(): void
    {
        $this->el = new ExpressionLanguage();
    }

    public static function providerIntInvalid(): array
    {
        return [
            ['int() function expected scalar, got null', 'int(null)'],
            ['int() function expected scalar, got array', 'int([])'],
        ];
    }

    #[DataProvider('providerIntInvalid')]
    public function testIntInvalid(string $expectedMessage, string $expression): void
    {
        $this->expectExceptionMessage($expectedMessage);
        $this->el->evaluate($expression);
    }

    public static function providerDateValid(): array
    {
        return [
            [0, 'int(false)'],
            [1, 'int(true)'],
            [0, 'int("abcd")'],
            [123, 'int("123")'],
            [0, 'int("0000.1")'],
            [830585094, 'int("830585094")'],
            [1234567890123456789, 'int("1234567890123456789")'], // max value
            [-34, 'int("-34")'],
            [456, 'int("   456    ")'],
        ];
    }

    #[DataProvider('providerDateValid')]
    public function testLengthValid(int $expected, string $expression): void
    {
        $int = $this->el->evaluate($expression);
        $this->assertIsInt($int);
        $this->assertEquals($expected, $int);
    }
}
