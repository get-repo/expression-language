<?php

namespace Test\Function;

use GetRepo\ExpressionLanguage\ExpressionLanguage;
use PHPUnit\Framework\TestCase;

abstract class FunctionTestCase extends TestCase
{
    protected ExpressionLanguage $el;

    public function setUp(): void
    {
        $this->el = new ExpressionLanguage();
    }
}
