<?php

namespace Test\Function;

use GetRepo\ExpressionLanguage\ExpressionLanguage;
use PHPUnit\Framework\Attributes\DataProvider;

class BetweenTest extends FunctionTestCase
{
    public function setUp(): void
    {
        $this->el = new ExpressionLanguage();
    }

    public static function providerDateValid(): array
    {
        return [
            'start match only' => ['bcd', 'between("a", 0, string)'],
            'end match only' => ['abc', 'between(0, "d", string)'],
            'full match between' => ['bc', 'between("a", "d", string)'],
            'no match' => ['abcd', 'between(0, 1, string)'],
        ];
    }

    #[DataProvider('providerDateValid')]
    public function testGetSuccess(string $expected, string $expression): void
    {
        $this->assertEquals($expected, $this->el->evaluate($expression, ['string' => 'abcd']));
    }
}
