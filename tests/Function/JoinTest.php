<?php

namespace Test\Function;

use GetRepo\ExpressionLanguage\ExpressionLanguage;
use PHPUnit\Framework\Attributes\DataProvider;

class JoinTest extends FunctionTestCase
{
    public function setUp(): void
    {
        $this->el = new ExpressionLanguage();
    }

    public static function providerDateValid(): array
    {
        return [
            'array empty' => ['', 'join("a", [])'],
            'array ok' => ['a b c d e f', 'join(" ", array)'],
            'separator empty' => ['abcdef', 'join("", array)'],
        ];
    }

    #[DataProvider('providerDateValid')]
    public function testGetSuccess(string $expected, string $expression): void
    {
        $this->assertEquals($expected, $this->el->evaluate($expression, ['array' => ['a', 'b', 'c', 'd', 'e', 'f']]));
    }
}
