<?php

namespace Test\Function;

use GetRepo\ExpressionLanguage\ExpressionLanguage;
use PHPUnit\Framework\Attributes\DataProvider;

class KeysTest extends FunctionTestCase
{
    public function setUp(): void
    {
        $this->el = new ExpressionLanguage();
    }

    public static function providerLength(): array
    {
        return [
            // empty
            [[], 'keys(null)'],
            [[], 'keys(false)'],
            [[], 'keys([])'],
            // true
            [[], 'keys(true)'],
            // arrays
            [[0], 'keys([1])'],
            [[0, 1, 2], 'keys([1, 2, 3])'],
            [['key'], 'keys({"key":"value"})'],
            [['array'], 'keys(array)', ['array' => ['array' => 'aaa']]],
            // strings
            [[], 'keys("123")'],
            [[], 'keys("")'],
            // object
            [[], 'keys(object)', ['object' => new class () {
            }]],
        ];
    }

    #[DataProvider('providerLength')]
    public function testLength(array $expected, string $expression, array $values = []): void
    {
        $this->assertEquals(
            $expected,
            $this->el->evaluate($expression, $values)
        );
    }
}
