<?php

namespace Test\Function;

use GetRepo\ExpressionLanguage\ExpressionLanguage;
use PHPUnit\Framework\Attributes\DataProvider;

class SplitTest extends FunctionTestCase
{
    public function setUp(): void
    {
        $this->el = new ExpressionLanguage();
    }

    public static function providerDateValid(): array
    {
        return [
            'string empty' => [[''], 'split("a", "")'],
            'separator empty' => [['abcdef'], 'split("", string)'],
            'separator invalid' => [['abcdef'], 'split("z", string)'],
            'separator invalid long' => [['abcdef'], 'split("abcdefghij", string)'],
            'separator first char' => [['', 'bcdef'], 'split("a", string)'],
            'separator second char' => [['a', 'cdef'], 'split("b", string)'],
            'separator last char' => [['abcde', ''], 'split("f", string)'],
            'string with spaces (no trim)' => [['  ab', 'def'], 'split("c", "  " ~ string)'],
        ];
    }

    #[DataProvider('providerDateValid')]
    public function testGetSuccess(array $expected, string $expression): void
    {
        $this->assertEquals($expected, $this->el->evaluate($expression, ['string' => 'abcdef']));
    }
}
