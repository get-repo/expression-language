<p align="center">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/16923926/expression_logo.png" height=100 />
</p>

<h1 align=center>Expression Language</h1>
<h4 align=center style="border-bottom: 3px solid #008033; padding-bottom: 15px;">
    Base on Symfony Expression Language Component
</h4>

<br/>
Relying on [Symfony Expression Language Component](https://symfony.com/doc/current/components/expression_language.html),
it can compile and evaluate expressions. An expression is a one-liner that returns a value (mostly, but not limited to, Booleans).
This library is just an extension of the Symfony Expression Language Component.

## Table of Contents

1. [Installation](#installation)
1. [Usage](#usage)
1. [Functions](#functions)
   - [get function](#get-function)
   - [set function](#set-function)
   - [has function](#has-function)
   - [keys function](#keys-function)
   - [length function](#length-function)
   - [range function](#range-function)
   - [replace function](#replace-function)
   - [trim function](#trim-function)
   - [date function](#date-function)
   - [int function](#int-function)
   - [between function](#between-function)
   - [join function](#join-function)
   - [split function](#split-function)

<br/><br/>
## Installation

This is installable via [Composer](https://getcomposer.org/):

    composer config repositories.get-repo/expression-language git https://gitlab.com/get-repo/expression-language.git
    composer require get-repo/expression-language

<br/><br/>
## Usage

The component provides 2 ways to work with expressions:
 - **Evaluation**: the expression is evaluated without being compiled to PHP;
 - **Compile**: the expression is compiled to PHP, so it can be cached and evaluated.
The main class of the component is `GetRepo\ExpressionLanguage\ExpressionLanguage`.

```php
use GetRepo\ExpressionLanguage\ExpressionLanguage;

$expressionLanguage = new ExpressionLanguage();

$expressionLanguage->evaluate('1 + 2'); // displays 3
$expressionLanguage->compile('1 + 2'); // displays (1 + 2)
```

<br/><br/>

## Functions

#### get function

The `get` function use the [Symfony Property Accessor Component](https://symfony.com/doc/current/components/property_access.html) to access values.
from objects or arrays:
```
// array = ['key' => 1];
get(array, "[key]") // return 1
```

#### set function

The `set` function use the [Symfony Property Accessor Component](https://symfony.com/doc/current/components/property_access.html) to set values.
from objects or arrays:
```
// array = ['key' => 1];
set(array, "[foo]", "bar") // array = ['key' => 1, 'foo' => 'bar']
```

#### has function

The `has` function use the [Symfony Property Accessor Component](https://symfony.com/doc/current/components/property_access.html) to check if value exists.
from objects or arrays:
```
// array = ['key' => 1];
has(array, "[foo]") // false
has(array, "[key]") // true
```

#### keys function

The `keys` function acts like the php function `array_keys()`.
```
// array = ['key' => 1];
keys(array) // ['key']
```

#### length function

The `length` function returns the size on an array ou string.
```
// array = ['a' => 1, 'b' => 2];
length(array) // 2
// string = 'abcd'
length(string) // 4
```

#### range function

The `range` function returns a range from a string (like when you print pages).
```
range("1") // [1]
range("1-3") // [1, 2, 3]
range("1,30,5") // [1, 30, 5]
range("55,3-5,10) // [55, 3, 4, 5, 10]
range("0-30 step 10") // [0, 10, 20, 30]
range("10-30 step 10,50-52,100") // [10, 20, 30, 50, 51, 52, 100]
```

#### replace function

The `replace` function replace in a string.
```
// string = 'I love symfony';
replace("symfony", "PHP", string) // 'I love PHP'
```

#### trim function

The `trim` function strips whitespace (or other chars) from the beginning and end of a string.
```
// string = '!  I like api-platform   !';
trim(string, " !") // 'I like api-platform'
```

#### date function

The `date` function converts a date string to a DateTime object.
```
date("Sat 18 May 2024") // will return a DateTime object, then we can apply format("Y-m-d") for instance
```

#### int function

The `int` function casts a string to an integer using the intval() PHP function.
```
int("567") // will return an 567 integer value
```

#### between function

The `between` function returns the content between start and end search strings.
```
between("a", "d", "abcd") // will return an "bc" string value
```

#### join function

The `join` function returns a string which is the concatenation of the items of a sequence.
```
join("-", [1,2,3]) // will return the "1-2-3" string value
```

#### split function

The `split` function splits a string by the given delimiter and returns a list of strings.
```
split(" ", "1 2 3") // will return the ["1", "2", "3"] array value
```
